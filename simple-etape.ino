// What pin to connect the sensor to
#define SENSORPIN A0 
 
void setup(void) {
  Serial.begin(9600);
}
 
void loop(void) {
  float reading;
 
  reading = analogRead(SENSORPIN);
 
  Serial.print("Analog reading "); 
  Serial.println(reading);
  Serial.print("analog zeroed ");
  reading = (reading - 494);
  Serial.println(reading);
  Serial.print("percent left ");
  reading = (reading / 250);
  Serial.println(reading * 100); 
    
  delay(1000);
}
