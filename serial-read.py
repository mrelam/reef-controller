import sys
import serial

port = "/dev/ttyACM0"
baud = 9600

ser = serial.Serial()
ser.port = port
ser.baudrate = baud

try:
   ser.open()
except:
   sys.stderr.write("Error opening serial port %s\n" % (ser.portstr) )
   sys.exit(1)

ser.setRtsCts(0)

while 1:
   # Read from serial port, blocking
   data = ser.readline()

   # If there is more than 1 byte, read the rest
   n = ser.inWaiting()
   if n:
       data = data + ser.read(n)

##   sys.stdout.write(data)
   try:
	f = open("/var/www/html/sensors/data.csv", "w")
	try:
	   f.writelines('PH,mS,S,PPM,SG,Back,Display,toP'"\n")
	   f.writelines(data)
	   f.write("\n")
##	f = open("/var/www/html/sensors/data.htm", "w")
##	try:
##	    f.write('<!DOCTYPE html >'"\n")
##	    f.write('<html>'"\n")
##	    f.write('<body>'"\n")
##	    f.write('<pre>PH,mS,S,PPM,SG,Back,Display</pre>'"\n")
##	    f.write('<pre>')
##	    f.writelines(data)
##	    f.write('</pre>'"\n")
##	    f.write('</body>')
	finally:
	    f.close()
   except IOError:
	pass
time.sleep(10)
