Reef Controller Project

Equipment used:

Arduino UNO r3
Tectacle Shield from WhiteBox Labs
PH and EC stamps from Atlas Scientific
Dallas/Maxim 18b20 Temp Sensors
Raspberry Pi 3
2.2" LCD on Pi


Scripts Used:

ScreenOutput.py  --  Graphical stats drawing on the RPi

screenshot.png   --  Screenshot of graphical stats

serial-read.py   --  Read serial port and dump to csv file

arduino-tentacle-2cir-2temp-csv.pde  --  Arduino code to read and report sensor readings over serial port.

test-influx.py  --  * Script in progress * Inserts serial data from arduino into influxdb

screen-counter.py  --  Simple screen counter (code for use in water change / feeding cycles)

serial-influx.py  -- testing serial read and post to influxdb

influx-json.py	--  deprecated json

arduino-out-sample.txt  --  sample output from arduino

serial-csv-output.txt  --  sample output from serial-read.py

sensors.csv  --  sample output data from arduino

sensors.htm -- temp file to make sure curl put is ok

reef-controller.png  --  Flow diagram of how things should work
