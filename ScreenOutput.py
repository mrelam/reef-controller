# Display stats for framebuffer1 LCD
# Nov 22 2014
# Updated: Jan 18 2016
# Adding living room temp and humidity
# Uses framebuffer 1
#
# Picture of how it looks: http://imgur.com/Hm9syVQ

import pygame, sys, os, time, datetime, urllib, csv
from pygame.locals import *
os.environ["SDL_FBDEV"] = "/dev/fb1"

## Globals

values = "NULL"
labels = "NULL"
timetopoll = True

pygame.init()

## Set up the screen

DISPLAYSURF = pygame.display.set_mode((320, 240), 0, 16)
pygame.mouse.set_visible(0)
pygame.display.set_caption('Stats')

# set up the colors
BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
RED   = (255,   0,   0)
GREEN = (  0, 255,   0)
BLUE  = (  0,   0, 255)
CYAN  = (  0, 255, 255)
ORANGE = (255,128,0)
GREY = (111,119,125)
PLUM = (234,173,234)
IVORY = (205,205,193)

## Main loop

while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    currentime = datetime.datetime.time(datetime.datetime.now())


## Poll temperature date from web server as CSV and parse it

    if timetopoll:
      try:
       tempdata = urllib.urlopen("http://127.0.0.1/sensors/data.csv")
       if tempdata.getcode() <= 299:
         reader = csv.reader(tempdata, delimiter=',')
         labels = reader.next()
         values = reader.next()
         timetopoll = False
         try:
           test = values[0]
           test = values[1]
           test = values[2]
           test = values[3]
           test = values[4]
           test = values[5]
           test = values[6]
           test = values[7]
         except:
           values = i
       else:
         values = "--.-", "--.-", "--.-", "--.-", "--.-", "--.-", "--.-", "--.-"
         timetopoll = False
      except:
       values = "--.-", "--.-", "--.-", "--.-", "--.-", "--.-", "--.-", "--.-"
       timetopoll = False

    else:
      #print "Not polling"
      timetopoll = True


## Draw the titles

    #graph = pygame.image.load("/root/lcd/background.png")
    #graphrect = graph.get_rect()
    #DISPLAYSURF.blit(graph, graphrect)

    black_square_that_is_the_size_of_the_screen = pygame.Surface(DISPLAYSURF.get_size())
    black_square_that_is_the_size_of_the_screen.fill((0, 0, 0))
    DISPLAYSURF.blit(black_square_that_is_the_size_of_the_screen, (0, 0))

    font = pygame.font.Font(None, 25)
    text = font.render("PH", 1, GREY)
    textpos = text.get_rect(left=DISPLAYSURF.get_width()/4-75)
    DISPLAYSURF.blit(text, textpos)

    font = pygame.font.Font(None, 25)
    text = font.render("Salinity", 1, GREY)
    textpos = text.get_rect(right=DISPLAYSURF.get_width()/4+155)
    DISPLAYSURF.blit(text, textpos)

    font = pygame.font.Font(None, 25)
    text = font.render("  Top Off Percent", 1, GREY)
    textpos = text.get_rect(bottom=DISPLAYSURF.get_width()/4+12)
    DISPLAYSURF.blit(text, textpos)

## Draw ph

    font = pygame.font.Font(None, 58)
    text = font.render(values[0], 1, IVORY)
    textpos = text.get_rect(center=(DISPLAYSURF.get_width()/4-10,45))
    DISPLAYSURF.blit(text, textpos)

##    font = pygame.font.Font(None, 20)
##    textF = font.render(u'\u00b0' + "F", 1, WHITE)
##    textposF = textpos[0] + textpos[2], textpos[1] + 10    
##    DISPLAYSURF.blit(textF, textposF)

## Draw Top off percent left

    font = pygame.font.Font(None, 58)
    text = font.render(values[7], 1, IVORY)
    textpos = text.get_rect(center=(DISPLAYSURF.get_width()/4-10,115))
    DISPLAYSURF.blit(text, textpos)

    font = pygame.font.Font(None, 18)
    textF = font.render(" %", 1, IVORY)
    textposF = textpos[0] + textpos[2], textpos[1] + 10
    DISPLAYSURF.blit(textF, textposF)


##    font = pygame.font.Font(None, 20)
##    textF = font.render(u'\u00b0' + "F", 1, WHITE)
##    textposF = textpos[0] + textpos[2], textpos[1] + 10    
##    DISPLAYSURF.blit(textF, textposF)
## Draw Lines

    pygame.draw.line(DISPLAYSURF, ORANGE, [5, 140], [DISPLAYSURF.get_width()-5,140], 3)

    pygame.draw.line(DISPLAYSURF, ORANGE, [5, 70], [DISPLAYSURF.get_width()/2,70], 3)

    pygame.draw.line(DISPLAYSURF, ORANGE, [DISPLAYSURF.get_width()/2, 5], [DISPLAYSURF.get_width()/2,140], 3)


## Ec (salinity SG) Sensor

    font = pygame.font.Font(None, 58)
    text = font.render(values[4], 1, IVORY)
    textpos = text.get_rect(topright=(DISPLAYSURF.get_width()/4+190,30))
    DISPLAYSURF.blit(text, textpos)

    font = pygame.font.Font(None, 18)
    textF = font.render(" SG", 1, IVORY)
    textposF = textpos[0] + textpos[2], textpos[1] + 10
    DISPLAYSURF.blit(textF, textposF)


## Ec Salinity number Sensor

    font = pygame.font.Font(None, 58)
    text = font.render(values[3], 1, IVORY)
    textpos = text.get_rect(topright=(DISPLAYSURF.get_width()/4+190,80))
    DISPLAYSURF.blit(text, textpos)

    font = pygame.font.Font(None, 18)
    textF = font.render(" ppm", 1, IVORY)
    textposF = textpos[0] + textpos[2], textpos[1] + 10
    DISPLAYSURF.blit(textF, textposF)


## Min

##    font = pygame.font.Font(None, 30)
##    text = font.render("Min:", 1, WHITE)
##    textpos = text.get_rect(topright=(DISPLAYSURF.get_width()/2-90,145))
##    DISPLAYSURF.blit(text, textpos)

##    font = pygame.font.Font(None, 30)
##    textF = font.render(values[2], 1, WHITE)
##  textF = font.render("981.8", 1, WHITE)
##    textposF = textpos[0] + textpos[2] + 10, textpos[1]
##    DISPLAYSURF.blit(textF, textposF)


## Max

##    font = pygame.font.Font(None, 30)
##    text = font.render("Max:", 1, WHITE)
##    textpos = text.get_rect(topright=(DISPLAYSURF.get_width()/2+90,145))
##    textpos = text.get_rect(topright=(DISPLAYSURF.get_width()/2-90,165))
##    DISPLAYSURF.blit(text, textpos)

##    font = pygame.font.Font(None, 30)
##    textF = font.render(values[3], 1, WHITE)
##  textF = font.render("1.8", 1, WHITE)
##    textposF = textpos[0] + textpos[2] + 10, textpos[1]
##    DISPLAYSURF.blit(textF, textposF)



## Temp Sensor in Display

    font = pygame.font.Font(None, 25)
    text = font.render("Display:", 1, GREY)
##    textpos = text.get_rect(topright=(DISPLAYSURF.get_width()/2-90,165))
    textpos = text.get_rect(topright=(DISPLAYSURF.get_width()/2-75,150))
    DISPLAYSURF.blit(text, textpos)

    font = pygame.font.Font(None, 25)
    textF = font.render(values[6], 1, IVORY)
##  textF = font.render("1.8", 1, WHITE)
    textposF = textpos[0] + textpos[2] + 10, textpos[1]
    DISPLAYSURF.blit(textF, textposF)

    font = pygame.font.Font(None, 20)
    textF = font.render(u'\u00b0' + "F", 1, IVORY)
    textposF = textpos[0] + textpos[2] + 55, textpos[1]    
    DISPLAYSURF.blit(textF, textposF)


## Temp Sensor in back compartment

    font = pygame.font.Font(None, 25)
    text = font.render("Back:", 1, GREY)
    textpos = text.get_rect(topleft=(DISPLAYSURF.get_width()/2+5,150))
    DISPLAYSURF.blit(text, textpos)

    font = pygame.font.Font(None, 25)
    textF = font.render(values[5], 1, IVORY)
    textposF = textpos[0] + textpos[2] + 10, textpos[1]
    DISPLAYSURF.blit(textF, textposF)

    font = pygame.font.Font(None, 20)
    textF = font.render(u'\u00b0' + "F", 1, IVORY)
    textposF = textpos[0] + textpos[2] + 55, textpos[1]    
    DISPLAYSURF.blit(textF, textposF)

## Draw time

    font = pygame.font.Font(None, 55)
    text = font.render(currentime.strftime("%H:%M"), 1, IVORY)
    textpos = text.get_rect(center=(DISPLAYSURF.get_width()/2,205))
    DISPLAYSURF.blit(text, textpos)


## Update the LCD

    pygame.display.update()    


## Sleep time!

    time.sleep(10)
