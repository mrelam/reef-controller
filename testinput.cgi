#!/usr/bin/perl
 
use strict;
use warnings;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use InfluxDB::LineProtocol qw(data2line line2data);
use Hijk;
my $q = new CGI;
 
print $q->header();
 
# Output stylesheet, heading etc
output_top($q);
 
if ($q->param()) {
    # Parameters are defined, therefore the form has been submitted
    display_results($q);
} else {
    # We're here for the first time, display the form
    output_form($q);
}
 
# Output footer and end html
output_end($q);
 
exit 0;
 
#-------------------------------------------------------------
 
# Outputs the start html tag, stylesheet and heading
sub output_top {
    my ($q) = @_;
    print $q->start_html(
        -title => 'Manual Testing Data',
        -bgcolor => 'white',
        -style => {
        -code => '
                /* Stylesheet code */
                body {
                    font-family: verdana, sans-serif;
                }
                h2 {
                    color: darkblue;
                    border-bottom: 1pt solid;
                    width: 100%;
                }
                div {
                    text-align: right;
                    color: steelblue;
                    border-top: darkblue 1pt solid;
                    margin-top: 4pt;
                }
                th {
                    text-align: right;
                    padding: 2pt;
                    vertical-align: top;
                }
                td {
                    padding: 2pt;
                    vertical-align: top;
                }
                /* End Stylesheet code */
            ',
    },
    );
    print $q->h2("Manual Testing Data");
}
 
# Outputs a footer line and end html tags
sub output_end {
    my ($q) = @_;
    print $q->div("My Web Form");
    print $q->end_html;
}
 
sub insert_influx {
    my $type  = shift;
    my $value = shift;
 
    # Create $line for inserting into InfluxDB
    my $line = data2line($type,$value);
 
    my $res = Hijk::request({
         method        => 'POST',
         host          => '192.168.200.130',
         port          => 8086,
         path          => "/write",
         query_string  => "db=ManualReef",
         body          => "$line"
    });
}
 
# Displays the results of the form
sub display_results {
    my ($q) = @_;
 
    foreach my $param ($q->param) {
      if ($param =~ '^input_') {
        # Remove the leading 'input_' from the parameter name
        $param =~ s/^input_//;
        print $q->p("$param: ",$q->param("input_$param"));
        insert_influx ("$param: ",$q->param("input_$param"));
      }
    }
}
 
# Outputs a web form
sub output_form {
    my ($q) = @_;
    print $q->start_form(
        -name => 'main',
        -method => 'POST',
    );
    print $q->start_table;
    print $q->Tr(
      $q->td('Magnesium:'),
      $q->td(
        $q->textfield("input_Mg")
      )
    );
    print $q->Tr(
      $q->td('Calcium:'),
      $q->td(
        $q->textfield("input_Ca")
      )
    );
    print $q->Tr(
      $q->td('Alkalinity:'),
      $q->td(
        $q->textfield("input_dKh")
      )
    );
    print $q->Tr(
      $q->td('Phosphate:'),
      $q->td(
        $q->textfield("input_P04")
      )
    );
    print $q->Tr(
      $q->td('Nitrate:'),
      $q->td(
        $q->textfield("input_N04")
      )
    );
    print $q->Tr(
      $q->td('Ammonia:'),
      $q->td(
        $q->textfield("input_Amm")
      )
    );
    print $q->Tr(
      $q->td($q->submit(-value => 'Submit')),
      $q->td('&nbsp;')
    );
    print $q->end_table;
    print $q->end_form;
}
