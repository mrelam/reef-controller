import sys
import serial
import time
import csv
import json

port = "/dev/ttyACM0"
baud = 9600
values = "NULL"
labels = "NULL"
timetopoll = True

ser = serial.Serial()
ser.port = port
ser.baudrate = baud
try:
   ser.open()
except:
   sys.stderr.write("Error opening serial port %s\n" % (ser.portstr) )
   sys.exit(1)

ser.setRtsCts(0)

while 1:
   # Read from serial port, blocking
    csvfile = ser.readline().decode('utf-8')[:-1]
    jsonfile = open('/var/www/html/sensors/data.htm', 'w')
    fieldnames = ("Ph", "mS", "TDS", "Salinity", "SG", "Back", "Display")
    reader = csv.DictReader(csvfile, fieldnames, delimiter=',')
    for row in reader:
   	print row
        json.dump(row, jsonfile)
        jsonfile.write('\n')
time.sleep(10)
