#!/usr/bin/python

import sys
import serial
import csv
import os
import StringIO

values = "NULL"

port = "/dev/ttyACM0"
baud = 9600

ser           = serial.Serial()
ser.port      = port
ser.baudrate  = baud

try:
    ser.open()
except:
    sys.stderr.write("Error opening serial port %s\n" % (ser.portstr) )
    sys.exit(1)

ser.setRtsCts(0)

datagarbage = ser.readline()
data = ser.readline()
#print data
reader = csv.reader(data.splitlines(), delimiter=',')
for value in reader:
    print '\t'.join(value)
#    print '\t'.join(value)
#
#print value[0]
#print value[1]
#print value[2]
#print value[3]
#print value[4]
#print value[5]
#print value[6]
#print value[7]

#print "my values are {0}, {1}, {4}, {6}".format(*value)

print "curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary 'Ph value={0}\nEC,units=mS value={1}\nEC,units=TDS value={2}\nEC,units=Salinity value={3}\nEC,units=SG value={4}\nTemp,location=Back value={5}\nTemp,location=Display value={6}\ntoP value={7}'" .format(*value)

os.system ("curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary 'Ph value={0}\nEC,units=mS value={1}\nEC,units=TDS value={2}\nEC,units=Salinity value={3}\nEC,units=SG value={4}\nTemp,location=Back value={5}\nTemp,location=Display value={6}\ntoP value={7}'" .format(*value))
#os.system ("curl -i -XPOST http://192.168.200.130:8086/write?db=Reef --data-binary 'Ph value={0}'" .format(*value))
#os.system (curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary EC,units=mS value={1} .format(*value))
#os.system (curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary EC,units=TDS value={2} .format(*value))
#os.system (curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary EC,units=Salinity value={3} .format(*value))
#os.system (curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary EC,units=SG value={4} .format(*value))
#os.system (curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary Temp,location=Back value={5} .format(*value))
#os.system (curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary Temp,location=Display value={6} .format(*value))
#os.system (curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary toP value={7} .format(*value))

#print ("curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary Ph value={0}" .format(*value))
#print ("curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary EC,units=mS value={1}" .format(*value))
#print ("curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary EC,units=TDS value={2}" .format(*value))
#print ("curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary EC,units=Salinity value={3}" .format(*value))
#print ("curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary EC,units=SG value={4}" .format(*value))
#print ("curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary Temp,location=Back value={5}" .format(*value))
#print ("curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary Temp,location=Display value={6}" .format(*value))
#print ("curl -i -XPOST 'http://192.168.200.130:8086/write?db=Reef' --data-binary toP value={7}" .format(*value))
f = open("/var/www/html/sensors/data.csv", "w")
f.writelines('PH,mS,S,PPM,SG,Back,Display,toP'"\n")
f.writelines("{0},{1},{2},{3},{4},{5},{6},{7}" .format(*value))
#f.write("\n")
f.close()
